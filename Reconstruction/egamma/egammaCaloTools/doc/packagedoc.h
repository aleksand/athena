/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**

@page egammaCaloTools_page egammaCaloTools Package
This package provides various tools related to calorimeter for egamma use.

*/
