/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_STGCREADOUTELEMENT_ICC
#define MUONREADOUTGEOMETRYR4_STGCREADOUTELEMENT_ICC


namespace ActsTrk{
    template <> inline Amg::Transform3D 
        TransformCacheDetEle<MuonGMR4::sTgcReadoutElement>::fetchTransform(const DetectorAlignStore* store) const{
            return  m_parent->toStation(store) * m_parent->fromGapToChamOrigin(hash());
        }
    template <> inline Identifier
        TransformCacheDetEle<MuonGMR4::sTgcReadoutElement>::identify() const {
            using RE = MuonGMR4::sTgcReadoutElement;
            return m_parent->measurementId(RE::createHash(RE::gasGapNumber(hash()) + 1,
                                                          RE::chType(hash()), 1));
    }
}


namespace MuonGMR4 {
    namespace sTgcIdMeasHashFields {
        constexpr unsigned int minusOne = -1;
        /// Hash field layout
        //// (wireInGrp)  | (channel)  [1-512]  | IsWireInGrp (0,1) |  (sTgcChannelType) [0-2] | (gasGap -1) [1-4]
        constexpr unsigned int gasGapShift = 0;
        constexpr unsigned int chTypeShift = 2;        
        constexpr unsigned int wireInGrpBit = chTypeShift+ 2;
        constexpr unsigned int chanShift = chTypeShift + 3;        
        constexpr unsigned int wireInGrpShift = chanShift + 9;
    }
    
inline double sTgcReadoutElement::chamberHeight() const { return 2.* m_pars.halfChamberHeight; }
inline double sTgcReadoutElement::sChamberLength() const { return 2.* m_pars.sHalfChamberLength; }
inline double sTgcReadoutElement::lChamberLength() const { return 2.* m_pars.lHalfChamberLength; }
inline double sTgcReadoutElement::thickness() const { return 2.* m_pars.halfChamberTck; }
inline double sTgcReadoutElement::sFrameWidth() const { return m_pars.sFrameWidth; }
inline double sTgcReadoutElement::lFrameWidth() const { return m_pars.lFrameWidth; }
inline int sTgcReadoutElement::multilayer() const { return m_multiLayer; }

inline unsigned int sTgcReadoutElement::numLayers() const { return m_pars.numLayers; }
inline unsigned int sTgcReadoutElement::nChTypes() const { return m_pars.nChTypes; }
inline double sTgcReadoutElement::gasGapThickness() const { return m_pars.gasTck; }
inline double sTgcReadoutElement::gasGapPitch() const { return m_gasGapPitch; }

inline double sTgcReadoutElement::firstStripPitch(const IdentifierHash& measHash) const { return m_pars.firstStripPitch[gasGapNumber(measHash)]; }
inline double sTgcReadoutElement::firstStripPitch(const Identifier& measId) const { return firstStripPitch(measurementHash(measId)); }

inline double sTgcReadoutElement::sGapLength(const Identifier& measId) const { return 2*stripDesign(measId).shortHalfHeight(); }
inline double sTgcReadoutElement::sGapLength(const IdentifierHash& measHash) const { return 2*stripDesign(measHash).shortHalfHeight(); }
inline double sTgcReadoutElement::lGapLength(const Identifier& measId) const { return 2*stripDesign(measId).longHalfHeight(); }
inline double sTgcReadoutElement::lGapLength(const IdentifierHash& measHash) const { return 2*stripDesign(measHash).longHalfHeight(); }
inline double sTgcReadoutElement::sPadLength(const Identifier& measId) const { return 2*padDesign(measId).shortHalfHeight(); }
inline double sTgcReadoutElement::sPadLength(const IdentifierHash& measHash) const { return 2*padDesign(measHash).shortHalfHeight(); }
inline double sTgcReadoutElement::lPadLength(const Identifier& measId) const { return 2*padDesign(measId).longHalfHeight(); }
inline double sTgcReadoutElement::lPadLength(const IdentifierHash& measHash) const { return 2*padDesign(measHash).longHalfHeight(); }
inline double sTgcReadoutElement::gapHeight(const Identifier& measId) const { return 2*stripDesign(measId).halfWidth(); }
inline double sTgcReadoutElement::gapHeight(const IdentifierHash& measHash) const { return 2*stripDesign(measHash).halfWidth(); }
inline double sTgcReadoutElement::yCutout(const Identifier& measId) const { return stripDesign(measId).yCutout(); }
inline double sTgcReadoutElement::yCutout(const IdentifierHash& measHash) const { return stripDesign(measHash).yCutout(); }

inline unsigned int sTgcReadoutElement::numStrips(const Identifier& measId) const { return stripDesign(measId).numStrips(); }
inline unsigned int sTgcReadoutElement::numStrips(const IdentifierHash& measHash) const { return stripDesign(measHash).numStrips(); }
inline double sTgcReadoutElement::stripPitch(const Identifier& measId) const { return stripDesign(measId).stripPitch(); }
inline double sTgcReadoutElement::stripPitch(const IdentifierHash& measHash) const { return stripDesign(measHash).stripPitch(); }
inline double sTgcReadoutElement::stripWidth(const Identifier& measId) const { return stripDesign(measId).stripWidth(); }
inline double sTgcReadoutElement::stripWidth(const IdentifierHash& measHash) const { return stripDesign(measHash).stripWidth(); }
inline double sTgcReadoutElement::stripLength(const Identifier& measId) const { unsigned int channel = m_idHelper.channel(measId);
                                                                                return stripDesign(measId).stripLength(channel); }
inline double sTgcReadoutElement::stripLength(const IdentifierHash& measHash) const { unsigned int channel = m_idHelper.channel(measurementId(measHash));
                                                                                      return stripDesign(measHash).stripLength(channel); }

inline double sTgcReadoutElement::wirePitch(const Identifier& measId) const { return wireDesign(measId).stripPitch(); }
inline double sTgcReadoutElement::wirePitch(const IdentifierHash& measHash) const { return wireDesign(measHash).stripPitch(); }
inline double sTgcReadoutElement::wireWidth(const Identifier& measId) const { return wireDesign(measId).stripWidth(); }
inline double sTgcReadoutElement::wireWidth(const IdentifierHash& measHash) const { return wireDesign(measHash).stripWidth(); }
inline unsigned int sTgcReadoutElement::wireGroupWidth(unsigned int gasGap) const { return wireDesign(gasGap).numWiresInGroup(2); }
inline unsigned int sTgcReadoutElement::numWires(unsigned int gasGap) const {return wireDesign(gasGap).nAllWires();}
inline unsigned int sTgcReadoutElement::numWireGroups(unsigned int gasGap) const { return wireDesign(gasGap).numStrips();}
inline unsigned int sTgcReadoutElement::firstWireGroupWidth(unsigned int gasGap) const { return wireDesign(gasGap).numWiresInGroup(1); }
inline double sTgcReadoutElement::wireCutout(unsigned int gasGap) const { return wireDesign(gasGap).wireCutout(); }

inline unsigned int sTgcReadoutElement::numPads(const Identifier& measId) const { return padDesign(measId).numPads(); }
inline unsigned int sTgcReadoutElement::numPads(const IdentifierHash& measHash) const { return padDesign(measHash).numPads(); }
inline unsigned int sTgcReadoutElement::numPadEta(const Identifier& measId) const { return padDesign(measId).numPadEta(); }
inline unsigned int sTgcReadoutElement::numPadEta(const IdentifierHash& measHash) const { return padDesign(measHash).numPadEta(); }
inline unsigned int sTgcReadoutElement::numPadPhi(const Identifier& measId) const { return padDesign(measId).numPadPhi(); }
inline unsigned int sTgcReadoutElement::numPadPhi(const IdentifierHash& measHash) const { return padDesign(measHash).numPadPhi(); }
inline double sTgcReadoutElement::firstPadHeight(const Identifier& measId) const { return padDesign(measId).firstPadHeight(); }
inline double sTgcReadoutElement::firstPadHeight(const IdentifierHash& measHash) const { return padDesign(measHash).firstPadHeight(); }
inline double sTgcReadoutElement::padHeight(const Identifier& measId) const { return padDesign(measId).padHeight(); }
inline double sTgcReadoutElement::padHeight(const IdentifierHash& measHash) const { return padDesign(measHash).padHeight(); }
inline double sTgcReadoutElement::padPhiShift(const Identifier& measId) const { return padDesign(measId).padPhiShift(); }
inline double sTgcReadoutElement::padPhiShift(const IdentifierHash& measHash) const { return padDesign(measHash).padPhiShift(); }
inline double sTgcReadoutElement::firstPadPhiDiv(const Identifier& measId) const { return padDesign(measId).firstPadPhiDiv(); }
inline double sTgcReadoutElement::firstPadPhiDiv(const IdentifierHash& measHash) const { return padDesign(measHash).firstPadPhiDiv(); }
inline double sTgcReadoutElement::anglePadPhi(const Identifier& measId) const { return padDesign(measId).anglePadPhi(); }
inline double sTgcReadoutElement::anglePadPhi(const IdentifierHash& measHash) const { return padDesign(measHash).anglePadPhi(); }
inline unsigned int sTgcReadoutElement::maxPadEta(const Identifier& measId) const { return padDesign(measId).anglePadPhi(); }
inline unsigned int sTgcReadoutElement::maxPadEta(const IdentifierHash& measHash) const { return padDesign(measHash).anglePadPhi(); }
inline unsigned int sTgcReadoutElement::padNumber(const Identifier& measId) const { return padNumber(measurementHash(measId)); }
inline unsigned int sTgcReadoutElement::padNumber(const IdentifierHash& measHash) const { return padDesign(measHash).padNumber(channelNumber(measHash)); }
inline std::pair<uint, uint> sTgcReadoutElement::padEtaPhi(const Identifier& measId) const { return padEtaPhi(measurementHash(measId)); }
inline std::pair<uint, uint> sTgcReadoutElement::padEtaPhi(const IdentifierHash& measHash) const { return padDesign(measHash).padEtaPhi(channelNumber(measHash)); }
inline unsigned int sTgcReadoutElement::padEta(const Identifier& measId) const { return padEta(measurementHash(measId)); }
inline unsigned int sTgcReadoutElement::padEta(const IdentifierHash& measHash) const { return padDesign(measHash).padEta(channelNumber(measHash)); }
inline unsigned int sTgcReadoutElement::padPhi(const Identifier& measId) const { return padPhi(measurementHash(measId)); }
inline unsigned int sTgcReadoutElement::padPhi(const IdentifierHash& measHash) const { return padDesign(measHash).padPhi(channelNumber(measHash)); }
inline double sTgcReadoutElement::beamlineRadius(const Identifier& measId) const { return padDesign(measId).beamlineRadius(); }
inline double sTgcReadoutElement::beamlineRadius(const IdentifierHash& measHash) const { return padDesign(measHash).beamlineRadius(); }
using localCornerArray = std::array<Amg::Vector2D, 4>;
inline localCornerArray sTgcReadoutElement::localPadCorners(const Identifier& measId) const { return localPadCorners(measurementHash(measId)); }
inline localCornerArray sTgcReadoutElement::localPadCorners(const IdentifierHash& measHash) const { return padDesign(measHash).padCorners(channelNumber(measHash)); }
using globalCornerArray = std::array<Amg::Vector3D, 4>;
inline globalCornerArray sTgcReadoutElement::globalPadCorners(const ActsGeometryContext& ctx, const Identifier& measId) const { 
    return globalPadCorners(ctx, measurementHash(measId)); 
}

inline const StripDesign& sTgcReadoutElement::stripDesign(const Identifier& measId) const {
    unsigned int gasGap = m_idHelper.gasGap(measId);
    return m_pars.stripLayers[gasGap-1].design();
 }
inline const StripDesign& sTgcReadoutElement::stripDesign(const IdentifierHash& measHash) const {
    return stripDesign(measurementId(measHash));
 }
inline const StripDesign& sTgcReadoutElement::stripDesign(unsigned int gasGap) const {
    return m_pars.stripLayers[gasGap-1].design();
 }

inline const WireGroupDesign& sTgcReadoutElement::wireDesign(const Identifier& measId) const {
    unsigned int gasGap = m_idHelper.gasGap(measId);
    return static_cast<const WireGroupDesign&>(m_pars.wireGroupLayers[gasGap-1].design());
 }
inline const WireGroupDesign& sTgcReadoutElement::wireDesign(const IdentifierHash& measHash) const {
    return wireDesign(measurementId(measHash));
 }
inline const WireGroupDesign& sTgcReadoutElement::wireDesign(unsigned int gasGap) const {
    return static_cast<const WireGroupDesign&>(m_pars.wireGroupLayers[gasGap-1].design());
 }

inline const PadDesign& sTgcReadoutElement::padDesign(const Identifier& measId) const {
    unsigned int gasGap = m_idHelper.gasGap(measId);
    return static_cast<const PadDesign&>(m_pars.padLayers[gasGap-1].design());
 }
inline const PadDesign& sTgcReadoutElement::padDesign(const IdentifierHash& measHash) const {
    return padDesign(measurementId(measHash));
 }
inline const PadDesign& sTgcReadoutElement::padDesign(unsigned int gasGap) const {
    return static_cast<const PadDesign&>(m_pars.padLayers[gasGap-1].design());
 }

inline IdentifierHash sTgcReadoutElement::measurementHash(const Identifier& measId) const {
    if (idHelperSvc()->detElId(measId) != identify()) {
        ATH_MSG_WARNING("The measurement " << idHelperSvc()->toString(measId)
                        << " picks the wrong readout element " << idHelperSvc()->toStringDetEl(identify()));
    }
    return createHash(m_idHelper.gasGap(measId),
                      m_idHelper.channelType(measId),
                      m_idHelper.channel(measId));
}
inline IdentifierHash sTgcReadoutElement::createHash(const unsigned int gasGap, 
                                                     const unsigned int channelType, 
                                                     const unsigned int channel,
                                                     const unsigned int wireInGrp) {
    using namespace sTgcIdMeasHashFields;
    /// Construct the Hash such that  (channel) | WireInGrpBit (0,1) | (channelType) | (gasGap -1) 
    if (channelType == ReadoutChannelType::WireInGrp) {
        const unsigned int readOutHash = static_cast<unsigned int>(createHash(gasGap, ReadoutChannelType::Wire, channel));
        return IdentifierHash {wireInGrp << wireInGrpShift |  (1<<wireInGrpBit) | readOutHash};
    }
    return IdentifierHash{ channel << chanShift | channelType << chTypeShift | (gasGap -1) };
}

inline IdentifierHash sTgcReadoutElement::layerHash(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << chanShift) | (1<<wireInGrpBit);
    return IdentifierHash{static_cast<unsigned int>(measHash) & (~mask)};
}

inline  unsigned int sTgcReadoutElement::channelNumber(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << wireInGrpShift);
    const unsigned int stripedHash = (~mask) & static_cast<unsigned int>(measHash);
    return stripedHash >> chanShift;
}
inline unsigned int sTgcReadoutElement::chType(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << chanShift);
    const unsigned int stripedHash = (~mask) & static_cast<unsigned int>(measHash);
    return stripedHash >> chTypeShift;
    
}
inline unsigned int sTgcReadoutElement::gasGapNumber(const IdentifierHash& measHash) {
    using namespace sTgcIdMeasHashFields;
    constexpr unsigned int mask = (minusOne << chTypeShift);
    return (static_cast<unsigned int>(measHash) &(~mask) );
}
inline Identifier sTgcReadoutElement::measurementId(const IdentifierHash& measHash) const {
    return m_idHelper.channelID(identify(), multilayer(), gasGapNumber(measHash) + 1, chType(measHash), channelNumber(measHash));  
}
inline IdentifierHash sTgcReadoutElement::layerHash(const Identifier& measId) const {
    if (m_idHelper.elementID(measId) != m_idHelper.elementID(identify()) ) {
        ATH_MSG_WARNING("The measurement " << idHelperSvc()->toString(measId)
                        << " picks the wrong readout element " << idHelperSvc()->toStringDetEl(identify()));
    }
    return createHash(m_idHelper.gasGap(measId), m_idHelper.channelType(measId), 0);
}

inline Amg::Vector2D sTgcReadoutElement::localChannelPosition(const Identifier& measId) const { 
    return localChannelPosition(measurementHash(measId)); }

inline Amg::Vector3D sTgcReadoutElement::globalChannelPosition(const ActsGeometryContext& ctx, const Identifier& measId) const {
    return globalChannelPosition(ctx, measurementHash(measId));
}

inline const StripLayer& sTgcReadoutElement::stripLayer(const IdentifierHash& measHash) const {
    unsigned int gasGap = gasGapNumber(measHash);
    return m_pars.stripLayers[gasGap];
}
inline const StripLayer& sTgcReadoutElement::stripLayer(const Identifier& measId) const {
    return stripLayer(measurementHash(measId));
}
inline int sTgcReadoutElement::padNumber(const Amg::Vector2D& hitPos, const Identifier& measId) const {
    return padNumber(hitPos, measurementHash(measId));
}

}  // namespace MuonGMR4
#endif
