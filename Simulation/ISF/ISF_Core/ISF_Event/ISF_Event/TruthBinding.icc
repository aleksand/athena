/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

// this file contains all ITruthBinding inline methods

namespace ISF {
  /** constructor setting all truth particle pointers to the given particle */
  TruthBinding::TruthBinding(HepMC::GenParticlePtr allTruthP) :
    m_truthParticle(allTruthP),
    m_primaryGenParticle(allTruthP),
    m_generationZeroGenParticle(allTruthP) { }

  /** constructor setting all truth particle pointers individually */
  TruthBinding::TruthBinding(HepMC::GenParticlePtr truthP, HepMC::GenParticlePtr primaryTruthP, HepMC::GenParticlePtr genZeroTruthP) :
    m_truthParticle(truthP),
    m_primaryGenParticle(primaryTruthP),
    m_generationZeroGenParticle(genZeroTruthP) { }

  /** destructor */
  TruthBinding::~TruthBinding() = default;

  /** comparison operator */
  bool TruthBinding::operator==(const TruthBinding& rhs) const {
    return isEqual(rhs);
  }

  /** check equality */
  bool TruthBinding::isEqual(const TruthBinding& rhs) const {
    bool pass = true;

    const auto rhsTruth = rhs.getCurrentGenParticle();
    if (m_truthParticle && rhsTruth) {
#ifdef HEPMC3
      pass &= (m_truthParticle == rhsTruth);
#else
      pass &= *m_truthParticle == *rhsTruth;
#endif
    } else {
      return false;
    }

    const auto rhsPrimary = rhs.getPrimaryGenParticle();
    if (m_primaryGenParticle && rhsPrimary) {
#ifdef HEPMC3
      pass &= (m_primaryGenParticle == rhsPrimary);
#else
      pass &= *m_primaryGenParticle == *rhsPrimary;
#endif
    } else {
      return false;
    }

    const auto rhsGenZero = rhs.getGenerationZeroGenParticle();
    if (m_generationZeroGenParticle && rhsGenZero) {
#ifdef HEPMC3
      pass &= (m_generationZeroGenParticle == rhsGenZero);
#else
      pass &= *m_generationZeroGenParticle == *rhsGenZero;
#endif
    } else {
      return false;
    }

    return pass;
  }

  /** check identity */
  bool TruthBinding::isIdent(const TruthBinding& rhs) const {
    bool pass = true;
    pass &= m_truthParticle == rhs.getCurrentGenParticle();
    pass &= m_primaryGenParticle == rhs.getPrimaryGenParticle();
    pass &= m_generationZeroGenParticle == rhs.getGenerationZeroGenParticle();
    return pass;
  }

  /** pointer to the particle in the simulation truth */
  HepMC::GenParticlePtr TruthBinding::getCurrentGenParticle() { return m_truthParticle; }
  HepMC::ConstGenParticlePtr TruthBinding::getCurrentGenParticle() const { return m_truthParticle; }
  void                TruthBinding::setCurrentGenParticle(HepMC::GenParticlePtr p) { m_truthParticle = p; }

  /** pointer to the primary particle in the simulation truth */
  HepMC::GenParticlePtr TruthBinding::getPrimaryGenParticle() { return m_primaryGenParticle; }
  HepMC::ConstGenParticlePtr TruthBinding::getPrimaryGenParticle() const { return m_primaryGenParticle; }

  /** pointer to the simulation truth particle before any regeneration (eg. brem) */
  HepMC::GenParticlePtr TruthBinding::getGenerationZeroGenParticle() { return m_generationZeroGenParticle; }
  HepMC::ConstGenParticlePtr TruthBinding::getGenerationZeroGenParticle() const { return m_generationZeroGenParticle; }
  void                TruthBinding::setGenerationZeroGenParticle(HepMC::GenParticlePtr p) { m_generationZeroGenParticle = p; }

  /** Create a TruthBinding for a child particle */
  // Not const: otherwise it can trigger a thread-safety checker warning
  // because the non-const m_primaryTruthParticle escapes.
  TruthBinding* TruthBinding::childTruthBinding(HepMC::GenParticlePtr childP) {
    return new TruthBinding(childP, m_primaryGenParticle, childP);
  }

} // end ISF namespace
